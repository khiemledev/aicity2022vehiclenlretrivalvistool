import { fromImage } from 'imtool'

const drawBBox = async (image: Blob, bboxes: number[], texts?: string) => {
  // if (!bboxes || bboxes.length === 0) return image
  // if (texts && bboxes.length !== texts.length) return image
  const img = await fromImage(image)
  const canvas = await img.toCanvas()
  const ctx = canvas.getContext('2d')
  if (!ctx) return
  // uncoment to draw multiple boxes, type of bboxes in this 
  // case will be number[][]
  // for (let i = 0; i < bboxes.length; i++) {
  let bb = bboxes
  ctx.beginPath()
  ctx.strokeStyle = 'red'
  ctx.lineWidth = 5
  // Draw rectangle
  // if (bb.length === 4) {
  //   ctx.strokeRect(bb[0], bb[1], bb[2] - bb[0], bb[3] - bb[1])
  // } else if (bb.length === 8) {
  //   let minx = Math.min(bb[0], bb[2], bb[4], bb[6])
  //   let maxx = Math.max(bb[0], bb[2], bb[4], bb[6])
  //   let miny = Math.min(bb[1], bb[3], bb[5], bb[7])
  //   let maxy = Math.max(bb[1], bb[3], bb[5], bb[7])
  //   ctx.strokeRect(minx, miny, maxx - minx, maxy - miny)
  // }
  // ctx.stroke()

  // Draw polygon
  ctx.beginPath()
  ctx.moveTo(bb[0], bb[1])
  ctx.lineTo(bb[2], bb[3])
  ctx.lineTo(bb[4], bb[5])
  ctx.lineTo(bb[6], bb[7])
  ctx.closePath()
  ctx.stroke()

  // Draw text
  // if (texts) {
  //   let { width, height } = img
  //   let size = Math.min(width, height) * 0.05
  //   customElements
  //   ctx.font = `bold ${size}px Arial`
  //   ctx.fillStyle = 'yellow'
  //   ctx.fillText(texts[i], bb[0] - 5, bb[1])
  // }
  // }
  return await (await fromImage(canvas.toDataURL())).toFile('img.jpg')
}

export { drawBBox }