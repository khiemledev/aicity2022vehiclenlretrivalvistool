import Image from 'next/image'
import { useContext, useEffect, useState } from 'react'
import axios from '../../src/axios_instance'
import { toast } from 'react-toastify'
import { drawBBox } from '../../src/image_util'
import { AppContext, Track } from '../contexts/AppContext'

const getTracks = async () => {
  let result: undefined | string[]
  try {
    const res = await axios.get<string[]>('/get-tracks')
    if (res.status !== 200) return
    result = res.data
  } catch (err) {
    console.log(err)
  } finally {
    return result
  }
}

const getTrack = async (trackId: string) => {
  try {
    const res = await axios.get<Track>('/get-track', {
      params: {
        trackId,
      }
    })
    if (res.status !== 200) return
    return res.data
  } catch (err) {
    console.log(err)
  }
}

const getImageURL = (framePath: string) => {
  return axios.defaults.baseURL + "/get-image?framePath=" + framePath
}

const getImage = async (url: string, bb: number[]) => {
  try {
    const res = await axios.get(url, {
      responseType: 'blob',
    })
    if (res.status !== 200) return
    let image = res.data
    image = drawBBox(image, bb)
    return image
  } catch (err) {
    console.error(err)
  }
}

const prevIcon = 
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="h-10 w-10"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
    strokeWidth={2}>
    <path
    strokeLinecap="round"
    strokeLinejoin="round"
    d="M11 15l-3-3m0 0l3-3m-3 3h8M3 12a9 9 0 1118 0 9 9 0 01-18 0z" />
  </svg>

const nextIcon =
  <svg xmlns="http://www.w3.org/2000/svg"
    className="h-10 w-10"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
    strokeWidth={2}>
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z" />
  </svg>

const Home = () => {
  const { tracks, setTracks } = useContext(AppContext)
  
  const [curTrackIdx, setCurTrackIdx] = useState<number>(0)
  const [curTrack, setCurTrack] = useState<Track | null>(null)
  const [curFrameIdx, setCurFrameIdx] = useState<number>(0)
  const [imageSrc, setImageSrc] = useState<string>()

  const _nextFrame = async () => {
    if (!curTrack) return
    if (curFrameIdx == curTrack.frames.length - 1) {
      toast.error('You reach the end frame of this track')
      return
    }
    const newFrameIdx = curFrameIdx + 1
    setCurFrameIdx(newFrameIdx)
    await fetchImageAndDrawBB(newFrameIdx, curTrack)
  }

  const _prevFrame = async () => {
    if (!curTrack) return
    if (curFrameIdx == 0) {
      toast.error('You reach the start frame of this track')
      return
    }
    const newFrameIdx = curFrameIdx - 1
    setCurFrameIdx(newFrameIdx)
    await fetchImageAndDrawBB(newFrameIdx, curTrack)
  }

  const _prevTrack = async () => {
    if (curTrackIdx === 0) {
      toast.error('You reach the start of the track list')
      return
    }
    const newTrackId = curTrackIdx - 1
    setCurTrackIdx(newTrackId)
    setCurFrameIdx(0)
    await fetchTrack(tracks[newTrackId])
  }

  const _nextTrack = async () => {
    if (!tracks) return
    if (curTrackIdx === tracks.length - 1) {
      toast.error('You reach the end of the track list')
      return
    }
    const newTrackId = curTrackIdx + 1
    setCurTrackIdx(newTrackId)
    setCurFrameIdx(0)
    await fetchTrack(tracks[newTrackId])
  }

  const fetchImageAndDrawBB = async (
    frameIdx: number,
    currentTrack: Track,  
  ) => {
    let bb = currentTrack.boxes[frameIdx]
    const [xc, yc, w, h] = bb
    const bbox = [
      xc, yc,
      xc + w, yc,
      xc + w, yc + h,
      xc, yc + h,
    ]
    const image = await getImage(
      getImageURL(currentTrack.frames[frameIdx]),
      bbox,
    )
    setImageSrc(URL.createObjectURL(image))
  }

  const fetchTrack = async (trackId: string) => {
    const track = await getTrack(trackId)
    if (!track) return
    setCurTrack(track)

    await fetchImageAndDrawBB(curFrameIdx, track)
  }

  useEffect(() => {
    let mounted = true

    const fetchTracks = async () => {
      const data = await getTracks()
      if (!data) return
      if (!mounted) return
      setTracks(data)

      await fetchTrack(data[curFrameIdx])
    }
    
    fetchTracks()

    return () => {
      mounted = false
    }
  }, [])
  
  return (
    <div className="grid grid-cols-12 w-full h-screen">
      <div className="space-y-10 bg-gray-100">
        <div 
          onClick={_prevTrack}
          className="flex flex-col items-center cursor-pointer"
        >
          {prevIcon}
          <p>Prev track</p>
        </div>
        <div
          onClick={_prevFrame}
          className="flex flex-col items-center cursor-pointer"
        >
          {prevIcon}
          <p>Prev frame</p>
        </div>
      </div>

      <div
        className="w-full h-full col-span-10"
      >
        {curTrack && (
          <div className="w-full h-full relative top-0 left-0">
            <div className="relative h-3/4">
              {imageSrc ? 
                <Image
                  unoptimized
                  src={imageSrc}
                  objectFit="fill"
                  layout="fill"
                />
                : null
              }
            </div>

            <div className="w-full h-1/4 overflow-y-scroll">
              <div className="space-y-3 p-2">
                <div className="text-xl font-bold">Natual language (NL)</div>
                <div>
                  {curTrack.nl.map(((nl, idx) => (
                    <p key={idx}>{nl}</p>
                  )))}
                </div>
              </div>

              <div className="space-y-3 p-2">
                <div className="text-xl font-bold">Natual language other views</div>
                <div>
                  {curTrack.nl_other_views.map(((nl, idx) => (
                    <p key={idx}>{nl}</p>
                  )))}
                </div>
              </div>
            </div>
          </div>
        )}

        <div className="flex items-center justify-center h-full">
          <div className="lds-dual-ring"></div>
        </div>
      </div>
      
      <div className="space-y-10 bg-gray-100">
        <div
          onClick={_nextTrack}
          className="flex flex-col items-center cursor-pointer"
        >
          {nextIcon}
          <p>Next track</p>
        </div>
        <div
          onClick={_nextFrame}
          className="flex flex-col items-center cursor-pointer"
        >
          {nextIcon}
          <p>Next frame</p>
        </div>
      </div>
    </div>
  )
}

export default Home