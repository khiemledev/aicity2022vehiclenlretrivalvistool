import type { NextApiRequest, NextApiResponse } from 'next'

import { testQueries } from './_init'

const handler = async (
  req: NextApiRequest, 
  res: NextApiResponse,
) => {
  const { queryId } = req.query

  const testQuery = testQueries[queryId as string]

  res.json(testQuery)
}

export default handler

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '10mb',
    },
  },
}