import fs from 'fs'

type Data = {
  [key: string]: any
}

const readFile = (path: string) => {
  const data = fs.readFileSync(path)
    .toString()

  const result: Data = JSON.parse(data)
  return result
}

const trainTracks = readFile('/mlcv/Databases/NvidiaAIC2022/Track2/train_tracks.json')
const testTracks = readFile('/mlcv/Databases/NvidiaAIC2022/Track2/test_tracks.json')
const testQueries = readFile('/mlcv/Databases/NvidiaAIC2022/Track2/test_queries.json')
const results = readFile('/mlcv/WorkingSpace/Personals/thuyendt/AICity2021-Track5/submit.json')

export { trainTracks, testTracks, testQueries, results }