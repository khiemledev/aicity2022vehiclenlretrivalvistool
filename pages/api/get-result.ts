import type { NextApiRequest, NextApiResponse } from 'next'

import { results } from './_init'

const handler = async (
  req: NextApiRequest, 
  res: NextApiResponse,
) => {
  const { queryId } = req.query

  const result = results[queryId as string]

  res.json(result)
}

export default handler

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '10mb',
    },
  },
}