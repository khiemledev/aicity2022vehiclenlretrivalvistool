import type { NextApiRequest, NextApiResponse } from 'next'

import { testQueries } from './_init'

const handler = async (
  req: NextApiRequest, 
  res: NextApiResponse,
) => {
  const result = Object.keys(testQueries)
  res.json(result)
}

export default handler

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '10mb',
    },
  },
}