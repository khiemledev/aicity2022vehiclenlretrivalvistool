import type { NextApiRequest, NextApiResponse } from 'next'

import { trainTracks, testTracks } from './_init'

const handler = async (
  req: NextApiRequest, 
  res: NextApiResponse,
) => {
  const { trackId, set } = req.query

  const tracks = set === 'test' ? testTracks : trainTracks

  const trackData = tracks[trackId as string]

  res.json(trackData)
}

export default handler

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '10mb',
    },
  },
}