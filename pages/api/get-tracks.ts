import type { NextApiRequest, NextApiResponse } from 'next'

import { trainTracks, testTracks } from './_init'

const handler = async (
  req: NextApiRequest, 
  res: NextApiResponse,
) => {
  const { set } = req.query
  const result = Object.keys(set === 'test' ? testTracks : trainTracks)
  res.json(result)
}

export default handler

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '10mb',
    },
  },
}