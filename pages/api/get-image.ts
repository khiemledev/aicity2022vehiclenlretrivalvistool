import type { NextApiRequest, NextApiResponse } from 'next'
import fs from 'fs'
import path from 'path'

const handler = async (
  req: NextApiRequest, 
  res: NextApiResponse,
) => {
  const { framePath } = req.query

  const basePath = '/mlcv/Databases/NvidiaAIC2022/Track2'
  const imgPath = path.join(basePath, framePath as string)

  try {
    if (!fs.existsSync(imgPath)) {
      return res.status(404).json({ message: 'Image not found' })
    }
    res.setHeader('Content-Type', 'image/jpeg')
    return res.send(fs.readFileSync(imgPath))
  } catch (e) {
    console.error(e)
    return res.json({ message: 'File not found' })
  }
}

export default handler

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '10mb',
    },
  },
}