import Head from 'next/head'
import { ToastContainer } from 'react-toastify'
import { VisResultProvider } from './contexts/VisResultContext'
import 'react-toastify/dist/ReactToastify.css'
import VisResult from './components/VisResult'

const App = () => {
  return (
    <>
      <Head>
        <title>AICity2022 Track 2 VisTool</title>
      </Head>

      <VisResultProvider>
        <VisResult />
      </VisResultProvider>

      <ToastContainer limit={1} position="top-center" />
    </>
  )
}

export default App