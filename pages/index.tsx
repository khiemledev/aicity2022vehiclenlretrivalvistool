import Head from 'next/head'
import { ToastContainer } from 'react-toastify'
import Home from './components/Home'
import 'react-toastify/dist/ReactToastify.css'
import { AppProvider } from './contexts/AppContext'

const App = () => {
  return (
    <>
      <Head>
        <title>AICity2022 Track 2 VisTool</title>
      </Head>

      <AppProvider>
        <Home />
      </AppProvider>

      <ToastContainer limit={1} position="top-center" />
    </>
  )
}

export default App