import { createContext, useState } from 'react'

type Track = {
  frames: string[],
  boxes: number[][],
  nl: string[],
  nl_other_views: string[]
}

type AppContextType = {
  tracks: string[],
  setTracks: (v: string[]) => void
}

const AppContext = createContext<AppContextType>({
  tracks: [],
  setTracks: () => {}
})

type Props = {
  children: React.ReactNode
}

const AppProvider = ({ children }: Props) => {
  const [tracks, setTracks] = useState<string[]>([])

  const value = {
    tracks,
    setTracks,
  }
  return <AppContext.Provider value={value}>
    {children}
  </AppContext.Provider>
}

export default AppProvider

export {
  AppContext,
  AppProvider,
}

export type {
  Track
}