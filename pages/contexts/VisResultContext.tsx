import { createContext, useState } from 'react'
import { Track } from './AppContext'

type Query = {
  nl: string[],
  nl_other_views: string[]
}

type VisResultContextType = {
  queries: string[],
  setQueries: (v: string[]) => void
  tracks: string[]
  setTracks: (v: string[]) =>void
}

const VisResultContext = createContext<VisResultContextType>({
  queries: [],
  setQueries: () => {},
  tracks: [],
  setTracks: () => {},
})

type Props = {
  children: React.ReactNode
}

const VisResultProvider = ({ children }: Props) => {
  const [queries, setQueries] = useState<string[]>([])
  const [tracks, setTracks] = useState<string[]>([])

  const value = {
    queries,
    setQueries,
    tracks,
    setTracks,
  }
  return <VisResultContext.Provider value={value}>
    {children}
  </VisResultContext.Provider>
}

export default VisResultProvider

export {
  VisResultContext,
  VisResultProvider,
}

export type {
  Query
}